package com.trailhead;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckTrails {
	WebDriver driver;
	public static Map ques_and_ans = new HashMap<String, String>();

	public void checkAnswers(WebDriver driver, String Link, String Innerlink, String answer) {
		try {
			this.driver = driver;
			LoginTrailhead.doWait(driver, 12000);

			WebElement mediumelement = driver.findElement(By.className("slds-medium-size_9-of-12"));

			List<WebElement> alllAAnswers = mediumelement.findElements(By.tagName("fieldset"));
			
			for (int k = 0; k < alllAAnswers.size(); k++) {

				if (alllAAnswers.get(k).findElement(By.className("slds-assistive-text")).getText().contains("correct"))

				{
					String option=alllAAnswers.get(k).findElement(By.className("slds-assistive-text")).getText();
					WriteAnswers.writeOptions(option, answer);
					

				}
			}

		} catch (Exception e) {
			System.out.println(e);
			return;
		}

	}

}
