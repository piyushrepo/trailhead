package com.trailhead;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginTrailhead {
	
	WebDriver driver;
	
	
	public LoginTrailhead(WebDriver driver) throws Exception {
		super();
		this.driver = driver;
		driver.get("https://trailblazer.me/TrailblazerLogin");
		LoginTrailhead.doWait(driver, 12000);
		WebElement gclick = driver.findElement(By.id("login_with_google"));
		gclick.click();
		LoginTrailhead.doWait(driver, 12000);
		WebElement email=driver.findElement(By.id("identifierId"));
        email.sendKeys("trailhead.miprocro");
        WebElement bNext = driver.findElement(By.id("identifierNext"));
		bNext.click();
		LoginTrailhead.doWait(driver, 12000);
		WebElement pass=driver.findElement(By.name("password"));
		pass.sendKeys("9860407635");
        WebElement passNext = driver.findElement(By.id("passwordNext"));
        passNext.click();
        LoginTrailhead.doWait(driver, 12000);
		WebElement rclick = driver.findElement(By.id("recovery_with_google"));
		rclick.click();   
	}
	
	
	public static void doWait(WebDriver driver,int l) throws Exception
	{
		synchronized (driver)
		{
		    driver.wait(l);
		}
	}
	
	
}
	