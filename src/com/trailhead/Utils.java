package com.trailhead;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Utils {
	public static void clearMap(Map<Object, Object> map) {
		map.clear();
	}

	public static void processMediumLink(WebElement alllmediumrinks, String innerlink, WebDriver driver,
			String mainlink) {
		try {
			if (LoadOnStartup.questrailslist.contains(alllmediumrinks.getText())) {
				driver.navigate().back();
				driver.navigate().refresh();
				return;
			} else {
				
				AutomateTrail.innerlink = alllmediumrinks.getText();
				driver.findElement(By.linkText(alllmediumrinks.getText())).click();
				System.out.println("automate trails 2");
				ProcessAnswers pa = new ProcessAnswers();
				pa.processTrails(driver, mainlink, innerlink);
				driver.navigate().back();
				LoginTrailhead.doWait(driver, 12000);
				driver.navigate().refresh();
				

			}
		} catch (Exception e) {
			driver.navigate().back();
			driver.navigate().refresh();
			return;
		}
	}

	public static void processInnerLink(WebElement alllinnerinks, String innerlink, WebDriver driver,
			String mainlink) {
		try {
			if (LoadOnStartup.innertrailslist.contains(alllinnerinks.getText())) {
				driver.navigate().back();
				driver.navigate().refresh();
				return;
			} else {
				AutomateTrail.badgelink=alllinnerinks.getText();
				driver.findElement(By.linkText(alllinnerinks.getText())).click();
				LoginTrailhead.doWait(driver, 12000);
				WebElement mediumelement = driver.findElement(By.className("slds-medium-size_3-of-4"));
				List<WebElement> alllmediumrinks = mediumelement.findElements(By.tagName("a"));
				for (int k = 0; k < alllmediumrinks.size(); k++) {
					if (k < 1) {
						Utils.processMediumLink(alllmediumrinks.get(k), innerlink, driver, mainlink);
					} else {

						WebElement mediumelementelse = driver.findElement(By.className("slds-medium-size_3-of-4"));
						List<WebElement> alllmediumrinksinforloop = mediumelementelse.findElements(By.tagName("a"));
						Utils.processMediumLink(alllmediumrinksinforloop.get(k), innerlink, driver, mainlink);

					}
				}
				driver.navigate().back();
				LoginTrailhead.doWait(driver, 12000);
				driver.navigate().refresh();
				
			}

		} catch (Exception e) {
			driver.navigate().back();
			driver.navigate().refresh();
			return;

		}
	}
	
	public static void  processAlllinks(WebElement alllinks, String innerlink, WebDriver driver,
			String mainlink) {
		try{
			if (alllinks.getText().length() < 10
					|| LoadOnStartup.maintrailslist.contains(alllinks.getText())) {
				driver.navigate().back();
				driver.navigate().refresh();
				return;
			} else {
				AutomateTrail.mainlink = alllinks.getText();
				System.out.println(alllinks.getText() + " " + alllinks.getText().length());
				driver.findElement(By.linkText(alllinks.getText())).click();
				LoginTrailhead.doWait(driver, 12000);
				System.out.println("came here in process all link 1 ");
				//WebElement darkelement = driver.findElement(By.className("th-bg--dark"));
				List<WebElement> alllinnerinks = driver.findElements(By.className("module-title"));
				//List<WebElement> alllinnerinks = darkelement.findElements(By.tagName("a"));
				System.out.println("came here in process all link 2");
				for (int j = 0; j < alllinnerinks.size(); j++) {
					if (j < 1) {
						System.out.println("came here in process all link 3");
					WebElement  modifiedinnerlink=(WebElement) alllinnerinks.get(j).findElements(By.tagName("a"));
					System.out.println("came here in process all link 34");
					Utils.processInnerLink(modifiedinnerlink, innerlink, driver, mainlink);
				}else
				{
					System.out.println("came here in process all link 4");
					List<WebElement> alllinnerinkselse = driver.findElements(By.className("module-title"));
					System.out.println("came here in process all link 42");
					WebElement  modifiedinnerlink=(WebElement) alllinnerinkselse.get(j).findElements(By.tagName("a"));
					System.out.println("came here in process all link 43");
					Utils.processInnerLink(modifiedinnerlink, innerlink, driver, mainlink);
					System.out.println("came here in process all link 44");
				}
				}
				driver.navigate().back();
				LoginTrailhead.doWait(driver, 12000);
				driver.navigate().refresh();
				// code to write processed trails to txt files
				//WriteProcessedTrails wpt = new WriteProcessedTrails(mainlink, badgelink, innerlink);
			}
			
		}catch(Exception e)
		{
			System.out.println("came here in process all link catch");
			driver.navigate().back();
			driver.navigate().refresh();
			return;
		}
		
	}
	}
