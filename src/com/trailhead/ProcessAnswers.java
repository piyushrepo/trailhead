package com.trailhead;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

public class ProcessAnswers {
	WebDriver driver;

	public void processTrails(WebDriver driver, String Link, String innerlink) {
		try {
			this.driver = driver;
			MoveTONextUnit mtnu = new MoveTONextUnit();
			driver.manage().window().maximize();

			LoginTrailhead.doWait(driver, 12000);

			WebElement mediumelement = driver.findElement(By.className("slds-medium-size_9-of-12"));

			List<WebElement> AllQuestions = mediumelement.findElements(By.tagName("fieldset"));
			List<WebElement> allloptionmain = AllQuestions.get(0).findElements(By.className("slds-radio_button"));
			WriteAnswers.writeTrails(Link, innerlink);
			for (int i = 0; i < allloptionmain.size(); i++) {
				for (int k = 0; k < AllQuestions.size(); k++) {

					if (!mtnu.navigateTONextUnit(driver)) {
						List<WebElement> allloptions = AllQuestions.get(k)
								.findElements(By.className("slds-radio_button"));
						allloptions.get(i).findElement(By.className("th-quiz__item-text")).click();
					}

				}
				driver.findElement(By.className("th-button--success")).click();
				CheckTrails ct = new CheckTrails();
				ct.checkAnswers(driver, Link, innerlink, String
						.valueOf(allloptionmain.get(i).findElement(By.className("th-quiz__item-text")).getText()));
			}
			WriteAnswers.addNewLine();
			/*
			 * MoveTONextUnit mtnu=new MoveTONextUnit(driver); ProcessAnswers
			 * pa=new ProcessAnswers(); pa.processTrails(driver,Link,innerlink);
			 */
		} catch (Exception e) {
			System.out.println(e);
			return;
		}
	}

}
