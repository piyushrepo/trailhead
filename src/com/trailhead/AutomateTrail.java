package com.trailhead;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AutomateTrail {
	static String mainlink = "", badgelink = "", innerlink = "";

	public static void main(String[] args) throws Exception {
		 try {
		// load the list of trails which are already done.
		LoadOnStartup ls = new LoadOnStartup();
		WebDriver driver = new FirefoxDriver();

		driver.manage().window().maximize();
		LoginTrailhead lth = new LoginTrailhead(driver);
		LoginTrailhead.doWait(driver, 20000);
		driver.findElement(By.linkText("Trails")).click();
		LoginTrailhead.doWait(driver, 20000);
		WebElement element = driver.findElement(By.className("th-bg--light"));
		List<WebElement> alllinks = element.findElements(By.tagName("a"));

		for (int i = 0; i < alllinks.size(); i++) {
			if (i < 1) {
				Utils.processAlllinks(alllinks.get(i), innerlink, driver, mainlink);
			} else {

				WebElement elementelse = driver.findElement(By.className("th-bg--light"));
				List<WebElement> alllinkselse = elementelse.findElements(By.tagName("a"));
				Utils.processAlllinks(alllinkselse.get(i), innerlink, driver, mainlink);
			}
			// code to write processed trails to txt files
			WriteProcessedTrails wpt = new WriteProcessedTrails(mainlink, badgelink, innerlink);
		}
		
		  } catch (Exception e) { System.out.println(e); }
		 
	}

}
