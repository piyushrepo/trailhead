package com.trailhead;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteProcessedTrails {
	String maintrails;
	String innertrails;
	String questrails;

	public WriteProcessedTrails(String maintrails, String innertrails, String questrails) {
		super();
		this.maintrails = maintrails;
		this.innertrails = innertrails;
		this.questrails = innertrails;
		writeMainTrails(maintrails);
		writeInnerTrails(innertrails);
		writeQuesTrails(questrails);

	}

	private void writeQuesTrails(String questrails) {
		try {

			// Open given file in append mode.
			BufferedWriter out = new BufferedWriter(new FileWriter("F:\\trailheadanswers\\questrails.txt", true));
			out.write(questrails);
			out.append('\n');
			out.close();
		} catch (IOException e) {
			System.out.println("exception occoured" + e);
			return;
		}

	}

	private void writeInnerTrails(String innertrails) {
		try {

			// Open given file in append mode.
			BufferedWriter out = new BufferedWriter(new FileWriter("F:\\trailheadanswers\\innertrails.txt", true));
			out.write(innertrails);
			out.append('\n');
			out.close();
		} catch (IOException e) {
			System.out.println("exception occoured" + e);
			return;
		}

	}

	private void writeMainTrails(String maintrails) {
		try {

			// Open given file in append mode.
			BufferedWriter out = new BufferedWriter(new FileWriter("F:\\trailheadanswers\\maintrails.txt", true));
			out.write(maintrails);
			out.append('\n');
			out.close();
		} catch (IOException e) {
			System.out.println("exception occoured" + e);
			return;
		}
	}

}
