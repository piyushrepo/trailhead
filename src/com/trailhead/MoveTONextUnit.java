package com.trailhead;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MoveTONextUnit {
	WebDriver driver;

	public boolean navigateTONextUnit(WebDriver driver) {
		try {
			this.driver = driver;
			LoginTrailhead.doWait(driver, 12000);
			if (driver.findElement(By.className("tds-button_brand")).isDisplayed()) {
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
		return false;
	}
}