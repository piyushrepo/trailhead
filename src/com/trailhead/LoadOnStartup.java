package com.trailhead;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class LoadOnStartup {
	public static Set maintrailslist = new HashSet<String>();
	public static Set innertrailslist = new HashSet<String>();
	public static Set questrailslist = new HashSet<String>();

	public LoadOnStartup() throws IOException {
		super();
		populateMainTrailslist();
		populateInnerTrailsList();
		populateQuestrailsList();
	}

	private void populateQuestrailsList() throws IOException {
		File file = new File("/home/piyush/trailheadsfile/questrails.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		while ((st = br.readLine()) != null) {
			questrailslist.add(st);
		}
	}

	private void populateInnerTrailsList() throws IOException {
		File file = new File("/home/piyush/trailheadsfile/innertrails.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		while ((st = br.readLine()) != null) {
			innertrailslist.add(st);
		}
	}

	private void populateMainTrailslist() throws IOException {
		File file = new File("/home/piyush/trailheadsfile/maintrails.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		while ((st = br.readLine()) != null) {
			maintrailslist.add(st);
		}

	}

}
